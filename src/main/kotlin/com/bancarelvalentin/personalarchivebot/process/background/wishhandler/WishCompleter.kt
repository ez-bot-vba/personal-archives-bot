package com.bancarelvalentin.personalarchivebot.process.background.wishhandler

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent

class WishCompleter(process: Process) : EventListener(process) {

    override fun onMessageReactionAdd(event: MessageReactionAddEvent) {
        super.onMessageReactionAdd(event)
        val channel = gateway.getTextChannelById(event.channel.id) ?: return
        if (channel.parentCategory?.idLong?.equals(HardCodedValues.CATEGORY_WISH_LISTS) == true
            && event.reaction.reactionEmote.emoji == "❓"
            && event.reaction.retrieveUsers().complete().map { it.idLong }.contains(HardCodedValues.USER__VBA)
        ) {
            completeTask(event)
            WishesLogic(gateway, process).wishMeta()
        }
    }

    companion object {

        /**
         * Complete the task present in [message]. If no message is present the one that has been reacted to will be used.
         */
        fun completeTask(event: MessageReactionAddEvent, message: Message = event.retrieveMessage().complete()) {
            message.clearReactions().queue()
            message.addReaction(EmojiEnum.OK_HAND.tounicode()).queue()
        }
    }
}