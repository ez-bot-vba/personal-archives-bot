package com.bancarelvalentin.personalarchivebot.process.background.safeplace

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import com.bancarelvalentin.personalarchivebot.HardCodedValues

class SafePlaceBot() : BackgroundProcess() {

    override val rawName = "Safe place"
    override val rawDesc = "Supprime les message dans le channel ${
        FormatingUtils.formatChannelId(HardCodedValues.CHANNEL_SAFE_PALCE)
    } 5 minutes aprés leur envoi ET une fois qu'il n'ont plus aucune reactions"

    override fun getListeners(): Array<EventListener> {
        return arrayOf(SafePlaceMessageListener(this), SafePlaceHandleReactionRemove(this))
    }


}