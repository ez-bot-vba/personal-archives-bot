package com.bancarelvalentin.personalarchivebot.process.background.wishhandler


import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess

class WishHandlerBot : BackgroundProcess() {

    override val rawName = "Wishes handler"
    override fun getListeners(): Array<EventListener> {
        return arrayOf(WishCreator(this), WishCompleter(this), WishReseter(this))
    }

    override val rawDesc =
        "Similaire à Task handler mais pour les channels de wish"


}
