package com.bancarelvalentin.personalarchivebot.process.background.safeplace


import com.bancarelvalentin.ezbot.config.EnvConfig
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.AsyncUtils
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import java.time.Duration

class SafePlaceMessageListener(process: Process) : EventListener(process) {

    val minimumKeptDuration = if (EnvConfig.DEV) Duration.ofSeconds(10) else Duration.ofMinutes(5)

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.channel.idLong != HardCodedValues.CHANNEL_SAFE_PALCE) return
        val unicode = EmojiEnum.LARGE_ORANGE_DIAMOND.tounicode()
        event.message.addReaction(unicode).complete()
        AsyncUtils.delay(minimumKeptDuration, Runnable {
            event.message.removeReaction(unicode, gateway.selfUser).complete()
        })
    }
}