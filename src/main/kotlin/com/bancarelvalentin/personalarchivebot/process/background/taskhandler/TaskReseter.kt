package com.bancarelvalentin.personalarchivebot.process.background.taskhandler

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent

class TaskReseter(process: Process) : EventListener(process) {

    override fun onMessageReactionAdd(event: MessageReactionAddEvent) {
        super.onMessageReactionAdd(event)
        val channel = gateway.getTextChannelById(event.channel.id) ?: return
        if (channel.parentCategory?.idLong?.equals(HardCodedValues.CATEGORY_TODO_LISTS) == true
            && event.reaction.reactionEmote.emoji == "\uD83D\uDC4E"
            && event.reaction.retrieveUsers().complete().map { it.idLong }.contains(HardCodedValues.USER__VBA)
        ) {
            val retrieveMessage = event.retrieveMessage().complete()
            retrieveMessage.clearReactions().complete()
            TaskCreator.create(retrieveMessage)
            ReminderLogic(gateway, process).remindMeta()
        }
    }

}