package com.bancarelvalentin.personalarchivebot.process.command.checker

import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import net.dv8tion.jda.api.entities.MessageChannel
import java.util.function.BiConsumer

class CheckerCommand : Command() {

    override val patterns = arrayOf("checker", "c")

    override val rawName = "Checker"
    override val rawDesc =
        "Affiche dans ${FormatingUtils.formatChannelId(HardCodedValues.CHANNEL_CHECK_META)} la liste de tous les checks "
    override val sample = "reminder"

    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(
        CheckerCommandChannelSelectedParam1::class.java,
        CheckerCommandChannelSelectedParam2::class.java,
        CheckerCommandChannelSelectedParam3::class.java,
        CheckerCommandChannelSelectedParam4::class.java,
        CheckerCommandChannelSelectedParam5::class.java
    )
    private val maxNumberOfChannels = 5

    override val logic = BiConsumer { request: CommandRequest, response: CommandResponse ->
        val channels = ArrayList<MessageChannel>()
        for (i in 0..maxNumberOfChannels) {
            try {
                channels.add(request.getParamChannel(i)!!)
            } catch (e: Exception) {
                break
            }
        }

        if (channels.isEmpty()) {
            throw NoCheckChannelProvided()
        }

        val checks = ArrayList<String>()
        channels.forEach {
            ChannelUtils.forEachMessageInChannel(
                it,
                logic = { message -> checks.add(message.contentRaw) })
        }


        val textChannel = gateway.getTextChannelById(HardCodedValues.CHANNEL_CHECK_META)
        ChannelUtils.emptyChannel(textChannel!!)
        for (msg in checks) {
            val complete = textChannel.sendMessage(msg).complete()
            complete.addReaction(EmojiEnum.RED_QUESTION_MARK.tounicode()).complete()
        }
    }
}
