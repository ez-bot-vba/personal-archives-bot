package com.bancarelvalentin.personalarchivebot.process.command.checker

import com.bancarelvalentin.ezbot.process.command.param.ChannelCommandParam

class CheckerCommandChannelSelectedParam3() : ChannelCommandParam() {

    override val optional = true
    override val rawDesc = "Un channel pour lequel générer la checklist"
    override val rawName = "Channel 3"

}
