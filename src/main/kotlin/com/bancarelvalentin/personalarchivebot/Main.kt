package com.bancarelvalentin.personalarchivebot

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.personalarchivebot.process.background.checkhandler.CheckHandlerBot
import com.bancarelvalentin.personalarchivebot.process.background.safeplace.SafePlaceBot
import com.bancarelvalentin.personalarchivebot.process.background.taskhandler.TaskHandlerBot
import com.bancarelvalentin.personalarchivebot.process.background.wishhandler.WishHandlerBot
import com.bancarelvalentin.personalarchivebot.process.command.checker.CheckerCommand
import com.bancarelvalentin.personalarchivebot.process.command.clearchecker.ClearCheckerCommand
import com.bancarelvalentin.personalarchivebot.process.scheduled.reminder.ReminderBot

// https://discord.com/oauth2/authorize?client_id=924787070852026399&scope=bot&permissions=8
// https://discord.com/api/oauth2/authorize?client_id=924787070852026399&scope=applications.commands
class Main {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {

            ConfigHandler.config = PersonalArchiveConfig()
            Orchestrator.add(WishHandlerBot())

            Orchestrator.add(TaskHandlerBot())

            Orchestrator.add(CheckHandlerBot())
            Orchestrator.add(CheckerCommand())
            Orchestrator.add(ClearCheckerCommand())

            Orchestrator.add(ReminderBot())
            Orchestrator.add(SafePlaceBot())
            Orchestrator.start()

            //val logger = com.bancarelvalentin.ezbot.logger.Logger()
            //val textChannelById = Orchestrator.gateway.getTextChannelById(1008781175030026291)
            //for (v in EmojiEnum.values()) {
            //    textChannelById!!.sendMessage(v.name + " | " + v.tounicode()).queue {
            //        it.addReaction(v.tounicode()).queue(
            //            { logger.warn(v.name + " | " + v.tounicode(), LogCategoriesEnum.DJ) },
            //            { logger.error(v.name + " | " + v.tounicode(), LogCategoriesEnum.DJ) }
            //        )
            //    }
            //}
        }
    }
}
