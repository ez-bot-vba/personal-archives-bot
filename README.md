# Bot

## Running

One environment variable must be set before run:

* **`DISCORD_API_TOKEN`** : Your discord bot API token


### Command line

`DISCORD_API_TOKEN="" java -jar Bot.jar`